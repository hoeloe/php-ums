<?php

include 'Entity.php';

class User extends Entity
{

    public $table_name = 'users';
    public $user_id;
    public $first_name;
    public $last_name;
    public $email;
    public $password_hash;
    public $secret_question;
    public $secret_answer;
    public $is_admin;
    public $blocked;
    public $admin_note;

    public function store() {
        $query = "INSERT INTO `users` (
                            first_name,
                            last_name,
                            email,
                            password_hash,
                            secret_question,
                            secret_answer
                        ) values (?, ?, ?, ?, ?, ?)";
        $params = [
            $this->first_name,
            $this->last_name,
            $this->email,
            $this->password_hash,
            $this->secret_question,
            $this->secret_answer,
        ];
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);
        $this->user_id = $this->pdo->lastInsertId();
    }

    public function load($id) {
        $query = "SELECT * FROM users WHERE user_id = ?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(array($id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->user_id = $id;
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->email = $data['email'];
        $this->password_hash = $data['password_hash'];
        $this->blocked = $data['blocked'];
        $this->secret_question = $data['secret_question'];
        $this->secret_answer = $data['secret_answer'];
        $this->is_admin = $data['is_admin'];

    }


    public function update($id) {
        $query = "UPDATE users 
                    SET 
                    email = ?,
                    first_name = ?,
                    last_name = ?,
                    secret_question = ?,
                    secret_answer = ? 
                    WHERE user_id = ?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(array(
            $this->email,
            $this->first_name,
            $this->last_name,
            $this->secret_question,
            $this->secret_answer,
            $this->user_id,
        ));
    }

    public function updatePassword($password_hash) {
        $query = "UPDATE users 
                    SET 
                    password_hash = ?
                    WHERE user_id = ?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(array(
            $password_hash,
            $this->user_id
        ));
    }

    public function idByEmail($email) {
        $stmt = $this->pdo->prepare("SELECT * FROM `users` WHERE email=?");
        $stmt->execute(array($email));
        $userRow = $stmt->fetch(PDO::FETCH_ASSOC);
        return $userRow['user_id'];
    }



    public function validateLogin() {
        $stmt = $this->pdo->prepare("SELECT * FROM `users` WHERE email=?");
        $stmt->execute(array($this->email));
        $userRow = $stmt->fetch(PDO::FETCH_ASSOC);
        $password1 = $_POST['password'];
        $password2 = $userRow['password_hash'];

        if ($stmt->rowCount() > 0) {
            if (password_verify($password1, $password2)) {
                $this->user_id = $userRow['user_id'];
                $this->load($this->user_id);
                return true;
            }
        }
        return false;

    }

    public function getQuestion($email) {
        $stmt = $this->pdo->prepare("SELECT * FROM `users` WHERE email=?");
        $stmt->execute(array($email));
        $userRow = $stmt->fetch(PDO::FETCH_ASSOC);
        return $userRow['secret_question'];
    }

    public function getQuestionByEmail($email) {
        return $this->getValue($email, 'secret_question', 'email' );
    }




}