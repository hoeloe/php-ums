<?php

abstract class Entity
{

    protected $pdo;
    protected $table_name;

    function __construct()
    {
        $config = parse_ini_file('config.ini');
        debug_to_console($config);
        $host = $config['host'];
        $db = $config['db'];
        $user = $config['user'];
        $password = $config['password'];
        $this->pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }


    public function updateValue($id, $column, $value, $idField = "id")
    {
        $query = "UPDATE $this->table_name SET $column = ? WHERE $idField = ?";
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute(array($value, $id));

    }

    public function getValue($id, $column = '*', $idField = "id")
    {
        $query = "SELECT $column FROM $this->table_name  WHERE $idField = ?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(array($id));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($column != '*') {
            return $result[$column];
        } else {
            return $result;
        }

    }

    public function getAll($columns = '*') {
        $stmt = $this->pdo->prepare("SELECT $columns FROM $this->table_name");
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }


}
