**PHP User Management System**
test:
- import database from database.sql
- set sql credentials in config.ini

working:

- register
- log in (email: admin@admin.nl, pw: admin)
- log out
- show/edit profile
- list users
- forgot/reset password
- admin block/make note/make admin

todo:
- security (admin sessions)
- cookies