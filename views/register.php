<?php
$controller->user = new User();
?>

<div class="register">
    <h3>Register new user</h3>
    <form method="post" action="index.php?action=registerNewUser">
        <p>First name: <input type="text" name="first_name" ></p>
        <p>Last name: <input type="text" name="last_name" ></p>
        <p>Email: <input type="text" name="email" ></p>
        <p>Password: <input type="password" name="password" ></p>
        <p>Please enter a secret question and answer for when you forget your password</p>
        <p>Question: <input type="text" name="secret_question" ></p>
        <p>Answer: <input type="text" name="secret_answer" ></p>
        <p><input type="submit" value="register"></p>
    </form>
</div>
