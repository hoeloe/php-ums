<?php

include 'models/User.php';

class Controller
{

    public $user;
    public $superUser;
    public $data;


    function __construct() {
        $this->user = new User();
        if (isset($_SESSION['user_id'])) {
            $this->user->load($_SESSION['user_id']);
        }
    }

    public function registerNewUser() {
        $user = new User();
        $user->email = $_POST['email'];
        $user->first_name = $_POST['first_name'];
        $user->last_name = $_POST['last_name'];
        $user->secret_question = $_POST['secret_question'];
        $user->secret_answer = $_POST['secret_answer'];
        $user->password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $user->store();
    }

    public function updateUser() {
        $this->user->secret_question = $_POST['secret_question'];
        $this->user->secret_answer = $_POST['secret_answer'];
        $this->user->email = $_POST['email'];
        $this->user->first_name = $_POST['first_name'];
        $this->user->last_name = $_POST['last_name'];
        $this->user->update($this->user->user_id);
    }

    public function logIn() {
        $this->user = new User();
        $this->user->email = $_POST['email'];
        if ($this->user->validateLogin()) {
            $_SESSION['user_id'] = $this->user->user_id;
            $_SESSION['first_name'] = $this->user->first_name;
            $user = $this->user;
            return  include 'views/profile.php';
        } else {
            return 'login failed';
        }
    }

    public function resetPassword() {
        $user = new User();
        $email = $_POST['email'];
        $password_hash = password_hash($_POST['new_password'], PASSWORD_DEFAULT);
        $user->load($user->idByEmail($email));
        if ($user->secret_answer == $_POST['secret_answer']) {
            $user->updatePassword($password_hash);
            return include "views/login.php";
        } else {
            return 'reset failed';
        }
    }

    public function logOut() {
        unset($_SESSION['user_id']);
        unset($_SESSION['first_name']);
        return include "views/login.php";
    }

    public function getQuestion() {
        $question = $this->user->getQuestion($_POST['email']);
        return [$question, include "views/passreset.php"];
    }

    public function makeAdmin() {
        $id = $_POST['user_id'];
        $user = new User();
        $user->updateValue($id, 'is_admin', 1, 'user_id');
        $this->userList();

    }

    public function revokeAdmin() {
        $id = $_POST['user_id'];
        $user = new User();
        $user->updateValue($id, 'is_admin', 0, 'user_id');
        $this->userList();

    }

    public function blockUser() {
        $id = $_POST['user_id'];
        $user = new User();
        $user->updateValue($id, 'blocked', 1, 'user_id');
        $this->userList();

    }

    public function unBlockUser() {
        $id = $_POST['user_id'];
        $user = new User();
        $user->updateValue($id, 'blocked', 0, 'user_id');
        $this->userList();

    }

    public function updateAdminNote() {
        debug_to_console('called');
        $id = $_POST['user_id'];
        $note = $_POST['admin_note'];
        $user = new User();
        $user->updateValue($id, 'admin_note', $note, 'user_id');
        $this->userList();
    }



    public function userList() {
        $users = $this->user->getAll();
        return include 'views/users.php';
    }


//    public function showProfile() {
//        $this->user = new User();
//        $this->user->load($_SESSION['user_id']);
//        include "views/profile.php";
//    }

    public function isLoggedIn() {
        if (isset($_SESSION['user_id'])) {
            return true;
        } else {
            return false;
        }
    }
}
