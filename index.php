<?php

session_start();

// --------------debugging------------
error_reporting(E_ALL);
ini_set("display_errors", 1);

function debug_to_console($data)
{
    $output = $data;
    if (is_array($output)) {
        $output = implode(',', $output);
    }

    echo "<script>console.log( 'Debug: " . $output . "' );</script>";
}

debug_to_console(strval($_REQUEST));


// -------------site code------------
require "Controller.php";
$controller = new Controller();

?>
<html>
    <head>
            <meta name="viewport" content="width=device-width, initial-scale=0.5"/>
            <meta charset="utf-8">
            <meta name="theme-color" content="#00CCCC">
            <title> user management</title>
        </head>
    <body>
    <h1>welcome to user the manager</h1>
    <?php




// TODO: fix users page
    if (isset($_GET['page'])) {
        $page = filter_var($_GET['page'], FILTER_SANITIZE_STRING);
        include "views/$page.php";
    } elseif (isset($_GET['action'])) {
        $action = filter_var($_GET['action'], FILTER_SANITIZE_STRING);
        call_user_func([$controller, $action], $_POST);
    } else {
        include 'views/home.php';
    }

    debug_to_console($controller->user->is_admin);
    if ($controller->user->is_admin != 0) { ?>
        <nav>
            <ul>
                <li><a href="?page=profile">profile</a></li>
                <li><a href="?page=register">register</a></li>
                <li><a href="?page=login">log in</a></li>
                <li><a href="?action=logOut">log out</a></li>
                <li><a href="?action=userList">user list</a></li>
                <li><a href="?page=passreset">reset password</a></li>
            </ul>
        </nav>
        <?php
    }





    ?>

    </body>
</html>
