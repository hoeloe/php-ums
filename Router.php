<?php

require "controllers/Controller.php";
class Router {

    private $request;

    function __construct($server_request) {
        $this->request = $server_request;
    }

    public function render() {
        $Controller = new Controller();
        if (isset($_GET['page'])) {
            switch ($_GET['page']) {
                case 'login':
                    $content = include 'views/login.php';
                    break;
                default:
                    $content = "<h1>hi</h1>";
            }

        } else {
            $content = 'no page to show';
        }

        return $content;
    }

    public function debug() {
//        return
    }

}