<div class="userlist">
    <style>td {border: 1px solid grey;}</style>`
    <table>
        <?php foreach ($users as $user) {
            $id = $user['user_id'];
            $note = $user['admin_note'];
            if ($user['is_admin']) {
                $isAdminField = "
<form action='?action=revokeAdmin' method='post'>
    <input type='hidden' value=$id name='user_id'>
    <input type='submit' value='Revoke admin status' style='background-color: purple; color: white'>
</form>";
            } else {
                $isAdminField = "
<form action='?action=makeAdmin' method='post'>
    <input type='hidden' value=$id name='user_id'>
    <input type='submit' value='Give admin status' style='background-color: white; color: black'>
</form>";

            }
            $noteField = "
<div>
    <form action='index.php?action=updateAdminNote' method='post'>
        <input type='hidden' value=$id name='user_id'>                            
        <textarea name='admin_note' id='' cols='30' rows='2' >$note</textarea>
        <input type='submit' value='update note'>
    </form>
</div>";
            if ($user['blocked']) {
                $blockedField = "
<form action='?action=unblockUser' method='post'>
    <input type='hidden' value=$id name='user_id'>                            
    <input type='submit' value='unblock' style='background-color: red'>
</form>";
            } else {
                $blockedField = "
<form action='?action=blockUser' method='post'>
    <input type='hidden' value=$id name='user_id'>                            
    <input type='submit' value='block' style='background-color: green'>
</form>";

            }
            ?>
            <tr>
                <td>id</td>
                <td>first name</td>
                <td>last name</td>
                <td>email</td>
                <td>secret question</td>
                <td>secret answer</td>
                <td>Is Admin</td>
                <td>admin note</td>
                <td>blocked</td>
            </tr>
            <tr>
                <td><?= $user['user_id'] ?></td>
                <td><?= $user['first_name'] ?></td>
                <td><?= $user['last_name'] ?></td>
                <td><?= $user['email'] ?></td>
                <td><?= $user['secret_question'] ?></td>
                <td><?= $user['secret_answer'] ?></td>
                <td><?= $isAdminField ?></td>
                <td><?= $noteField ?></td>
                <td><?= $blockedField ?></td>
            </tr>


        <?php } ?>
    </table>
</div>